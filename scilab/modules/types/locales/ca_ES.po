# Catalan translation for scilab
# Copyright (c) 2012 Rosetta Contributors and Canonical Ltd 2012
# This file is distributed under the same license as the scilab package.
# FIRST AUTHOR <EMAIL@ADDRESS>, 2012.
#
msgid ""
msgstr ""
"Project-Id-Version: scilab\n"
"Report-Msgid-Bugs-To: <localization@lists.scilab.org>\n"
"POT-Creation-Date: 2013-04-16 17:44+0100\n"
"PO-Revision-Date: 2012-04-19 18:10+0000\n"
"Last-Translator: Marc Coll Carrillo <Unknown>\n"
"Language-Team: Catalan <ca@li.org>\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-Generator: Launchpad (build 17865)\n"

#, c-format
msgid "%s: Wrong number of input argument(s): %d expected.\n"
msgstr ""

#, c-format
msgid "%s: Wrong number of input arguments: %d expected.\n"
msgstr ""

#, c-format
msgid "%s: Wrong type for input argument #%d: A scalar expected.\n"
msgstr ""

#, c-format
msgid "%s: Wrong size for input argument #%d: A scalar expected.\n"
msgstr ""

#, c-format
msgid "%s: Wrong number of input arguments: %d or %d expected.\n"
msgstr ""

#, c-format
msgid "%s: Wrong number of output argument(s): %d expected.\n"
msgstr ""

#, c-format
msgid ""
"%s: Wrong type for input argument #%d: struct array or tlist or mlist "
"expected.\n"
msgstr ""

#, c-format
msgid "%s: Wrong type for input argument #%d: string expected.\n"
msgstr ""

#, c-format
msgid "%s: Wrong size for input arguments: more than %d expected.\n"
msgstr ""

#, c-format
msgid "%s: Wrong size for output arguments: %d expected.\n"
msgstr ""

#, c-format
msgid ""
"%s: Wrong input arguments: Dimensions given as first argument do not match "
"specified cell contents.\n"
msgstr ""

#, c-format
msgid "%s: Wrong size for input argument #%d: A vector expected.\n"
msgstr ""

#, c-format
msgid "%s: Wrong number of input argument(s): An even number is expected.\n"
msgstr ""

#, c-format
msgid "%s: Field names must be strings.\n"
msgstr ""

#, c-format
msgid "%s: Arguments must be scalar or must have same dimensions.\n"
msgstr ""

#, c-format
msgid "%s: Wrong number of input arguments: At least %d expected.\n"
msgstr ""

#, c-format
msgid "%s: Wrong type for input argument #%d: String expected.\n"
msgstr ""

#, c-format
msgid "%s: Can not create a %s with input argument #%d.\n"
msgstr ""

#, c-format
msgid "%s: Wrong values for input argument #%d: '%s' expected .\n"
msgstr ""

msgid "Double"
msgstr "Doble"

msgid "Polynomial"
msgstr "Polinomi"

msgid "Boolean"
msgstr "Booleà"

msgid "Sparse"
msgstr "Matriu dispersa"

msgid "Boolean Sparse"
msgstr "Dispersa matriu booleana"

msgid "Matlab Sparse"
msgstr "Matriu dispersa de Matlab"

msgid "Integer"
msgstr "Enter"

msgid "Graphic handle"
msgstr ""

msgid "String"
msgstr "Cadena"

msgid "User function"
msgstr "Funció de l'usuari"

msgid "Compiled function"
msgstr "Funció compilada"

msgid "Function library"
msgstr "Funció de biblioteca"

msgid "List"
msgstr "Llista"

msgid "Tlist"
msgstr "Llista T"

msgid "Mlist"
msgstr "Llista M"

msgid "Pointer"
msgstr "Punter"

msgid "Implicit polynomial"
msgstr "Polinomi implícit"

msgid "Intrinsic function"
msgstr "Funció intrínseca"

msgid "Unknown datatype"
msgstr "Tipus de dades desconegut"
