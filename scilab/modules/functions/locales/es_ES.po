# Spanish translation for scilab
# Copyright (c) 2009 Rosetta Contributors and Canonical Ltd 2009
# This file is distributed under the same license as the scilab package.
# FIRST AUTHOR <EMAIL@ADDRESS>, 2009.
#
msgid ""
msgstr ""
"Project-Id-Version: scilab\n"
"Report-Msgid-Bugs-To: <localization@lists.scilab.org>\n"
"POT-Creation-Date: 2013-04-16 17:44+0100\n"
"PO-Revision-Date: 2015-10-08 22:03+0000\n"
"Last-Translator: Scilab.team <Unknown>\n"
"Language-Team: Spanish <es@li.org>\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-Generator: Launchpad (build 17865)\n"
"Language: es\n"

#, c-format
msgid "%s: Wrong number of input arguments: %d to %d expected.\n"
msgstr ""

#, c-format
msgid "%s: Wrong value for input argument #%d: 'errcatch' expected.\n"
msgstr ""
"%s: valor incorrecto de argumento de entrada nº %d. Se experaba un "
"'errcatch'.\n"

#, c-format
msgid "%s: Wrong type for input argument #%d: A integer expected.\n"
msgstr ""

#, c-format
msgid "%s: Wrong type for input argument #%d: A integer or string expected.\n"
msgstr ""

#, c-format
msgid "%s: Cannot open file %s.\n"
msgstr ""

#, c-format
msgid "%s: Unable to parse macro '%s'"
msgstr ""

#, c-format
msgid ""
"%s: Wrong type for input argument #%d: A macro without varargin and "
"varargout expected.\n"
msgstr ""

#, c-format
msgid "Recursion limit reached (%d).\n"
msgstr ""

#, c-format
msgid "%s: Wrong value for input argument #%d: '%s' or '%s' expected.\n"
msgstr ""

#, c-format
msgid "%s: Wrong type for input argument #%d: Vector of strings expected.\n"
msgstr ""

#, c-format
msgid "%s: Wrong number of input arguments: %d expected.\n"
msgstr ""

#, c-format
msgid "%s: Wrong number of output arguments: %d expected.\n"
msgstr ""

#, c-format
msgid "%s: Wrong number of output argument(s): %d expected.\n"
msgstr ""

#, c-format
msgid "%s: Wrong type for input argument #%d: A String expected.\n"
msgstr ""

#, c-format
msgid "%s: Wrong size for input argument #%d: A String expected.\n"
msgstr ""

#, c-format
msgid "%s: Invalid library %s.\n"
msgstr "%s: Biblioteca %s inválida.\n"

#, c-format
msgid "%s: Wrong type for input arguments: macro expected.\n"
msgstr ""

#, c-format
msgid "%s: Wrong number of input arguments: at least %d expected.\n"
msgstr ""

#, c-format
msgid "%s: Wrong type for input argument #%d.\n"
msgstr "%s: Tipo incorrecto del argumento de entrada #%d.\n"

#, c-format
msgid "%s: obsolete op-code %d.\n"
msgstr "%s: op-code %d obsoleto.\n"

#, c-format
msgid "Checking: %s.\n"
msgstr "Revisando: %s.\n"

#, c-format
msgid "%s: Please check: %s\n"
msgstr "%s: Por favor revise: %s\n"

#, c-format
msgid "%s: File %s doesn't exist.\n"
msgstr "%s: El archivo %s no existe.\n"

#, c-format
msgid "%s: Wrong number of input arguments: %d or %d expected.\n"
msgstr ""

#, c-format
msgid "%s: Wrong number of input argument: %d expected.\n"
msgstr ""

#, c-format
msgid "%s: Wrong size for input argument #%d: string expected.\n"
msgstr ""
"%s: Tamaño incorrecto del argumento de entrada #%d: Se esperaba un string.\n"

#, c-format
msgid "%s: I cannot find any files with extension %s in %s\n"
msgstr "%s: No puedo encontrar ningún archivo con la extensión %s en %s\n"

#, c-format
msgid "%s: Incorrect function in file %s.\n"
msgstr "%s: Función incorrecta en el archivo %s.\n"

#, c-format
msgid "%s: Wrong number of input argument(s): %d to %d expected.\n"
msgstr ""

#, c-format
msgid "%s: Wrong type for input argument #%d: A character string expected.\n"
msgstr ""
"%s: Tipo incorrecto del argumento de entrada #%d: Se esperaba un caracter "
"string.\n"

#, c-format
msgid "%s: Undefined variable %s.\n"
msgstr "%s: Variable indefinida %s.\n"

#, c-format
msgid ""
"%s: Wrong value for input argument #%d: Name of a Scilab function expected.\n"
msgstr ""
"%s: Valor incorrecto del argumento de entrada #%d: Se esperaba el nombre de "
"una función de Scilab.\n"

msgid "No comment available."
msgstr ""

#, c-format
msgid "%s: Wrong values for input argument: No profile data in the function\n"
msgstr ""
"%s: Valores incorrectos de argumentos de entrada: No hay perfil de datos en "
"la función\n"

msgid "Complexity"
msgstr "Complejidad"

msgid "Tools"
msgstr "Herramientas"

msgid "Edit"
msgstr "Editar"

msgid "?"
msgstr "?"

msgid "Exit"
msgstr "Salir"

msgid "Click to get corresponding line, move with a-z."
msgstr "Click para obtener la línea correspondiente, mover con a-z."

#, c-format
msgid "line %s [%s call, %s sec] :: "
msgstr "línea %s [%s llamado, %s seg] :: "

#, c-format
msgid "line %s [%s calls, %s sec] :: "
msgstr "línea %s [%s llamados, %s seg] :: "

#, c-format
msgid "%s: The function has not been built for profiling"
msgstr "%s: La función no fue construida para hacer perfiles"

#, c-format
msgid "No variable named: %s.\n"
msgstr "No hay una variable llamada: %s.\n"

#, c-format
msgid ""
"%s: Wrong value for input argument #%d: A Scilab function name is expected.\n"
msgstr ""
"%s: Valor incorrecto del argumento de entrada #%d: Se esperaba el nombre de "
"una función de Scilab.\n"

#, c-format
msgid "%s is already noncompiled.\n"
msgstr "%s ya está no-compilado.\n"

#, c-format
msgid "%s is already compiled.\n"
msgstr "%s ya está compilado.\n"

#, c-format
msgid "%s is already compiled for profiling.\n"
msgstr "%s ya está compilado para profiling.\n"

#, c-format
msgid "%s: Private function: cannot access to this function directly.\n"
msgstr ""
"%s: Función privada: no se puede acceder directamente a esta función.\n"

#, c-format
msgid "Feature %s is obsolete."
msgstr "La función %s es obsoleta."

#, c-format
msgid "Please use %s instead."
msgstr "Por favor use %s en su lugar."

#, c-format
msgid "This feature will be permanently removed in Scilab %s"
msgstr "Esta función será removida permanentemente de Scilab %s"
